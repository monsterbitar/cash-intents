**DISCLAIMER**: This is a work in progress

**STATE:** This is a draft used for discussion.


Protocol ID: 0x01010102


# Abstract

**Cash Intents** is a transactional metadata protocol that works in collaboration with the **Cash Accounts** protocol. Where **Cash Accounts** provides human meaning to payment information, **Cash Intents** provide human meaning to payment transactions.

# Motivation

There is a need today to create userfriendly interfaces but the technical data available on the Bitcoin Cash blockchain is limited to value being transferred and the validity of the transfer. Under the default transaction all human meaningful information about the transaction is abstracted away into technical identifiers and actions.

Storing the transactional metadata that gives a transaction meaning is beneficial to the vast majority of users.


# Specification

## Introduction

...

## Warning

This protocol is **NOT** fully trustless and some data stored with this protocol can be used for social engineering attacks. Wallets that implement this protocol **MUST** make sure that only verified data is presented as facts, and unverified data as a senders claim.

**Note:** *Recommendations for presentation of the various data is listed in the respective section that introduces the data.*

## Process

The creation of a **Cash Intent** is split into the following steps: **selection**, **commitment**, optional **encryption** and **transfer**.

### Selection

The senders wallet creates a transaction and gathers all necessary data for the desired **Intent Type**, either programmatically or as part of user input.

### Commitment

The senders wallet calculates a sha256 hash of the serialized **Commitment Parts** and signs it using the keypair that correspond to the senders **Cash Account**. 

Data | Description
--- | --- 
First Input Hash | The transaction hash of the first input of this transaction
First Input Index | The transaction input index of the first input of this transaction
Serialized Intent | The intent type and data, serialized in the same order as stored in the intent

**Note:** *The transactions first input is used as a nonce to prevent reuse of the signature*


### Encryption

The data in the intent can optionally be encrypted for increased privacy. When either the sender or recipient has indicated that they desire privacy, then data should be encrypted. The data can be encrypted with either the **Senders keypair** or a **Diffie-Hellman shared secret** between the **Senders keypair** and the **Recipients keypair**.

The **Senders keypair** corresponds with the first input of the transaction.

The **Recipients keypair** corresponds with the recipients payment data.

Sender | Recipient | Keypair
--- | --- | ---
No | Any | Don't encrypt
Yes | No | Sender keypair
Yes | Yes | Shared secret

**Note:** *Allowing the sender to encrypt in a way that only they can decrypt is mostly useful for backup purposes where restore-from-seed would otherwise have lost the human intent.*


### Transfer

The signed and optionally encrypted payload is attached to the transaction as an OP_RETURN output according to the following structure:

```
OP_RETURN (0x6a)
    OP_PUSH (0x04) <PROTOCOL_IDENTIFIER>
    OP_PUSH (0x01) <INTENT TYPE>
    OP_PUSH (0x??) <INTENT DATA> [...OP_PUSH (0x??) <INTENT DATA>]
    OP_PUSH (0x??) <SENDER SIGNATURE>
```


## Data types

Type | Identifier | Description
--- | --- | ---
Note | 0x01 | Message attached to the transaction.
User | 0x02 | Contains the sender and recipient only.
Transfer | 0x11 | Adds human meaning to existing technical identifiers.
Payment | 0x12 | Same as Transfer, but marked as a payment.
Donation | 0x13 | Same as Transfer, but marked as a donation.
Private Intent | 0xFE | Intent encrypted with the senders keypair.
Shared Intent | 0xFF | Intent encrypted with a shared keypair.

**Note:** **


### Note

Stores a text message to the transaction, in unicode (UTF-8) encoding.

Field | Size | Description
--- | --- | ---
Note | 1~148 bytes | Text message to attach to the transaction, in unicode (UTF-8)

*Wallet considerations:*

- In order to mitigate usefulness as spam, notes should only be displayed on transaction inspection.
- In order to mitigate security issues, notes must be treated as foreign input and assumed to be hostile.


### User

Stores who the sender and recipient of the transaction are, in a trustless and verifiable way.

Field | Size | Description
--- | --- | ---
sender | 32 bytes | transaction ID for the Cash Account registration that represent the sender
recipient | 32 bytes | transaction ID for the Cash Account registration that represent the recipient


### Transfer, Payment, Donation

Stores human readable metadata about a transactions value, participants and purpose.


Field | Size | Description
--- | --- | ---
sender | 32 bytes | transaction ID for the Cash Account registration that represent the sender
recipient | 32 bytes | transaction ID for the Cash Account registration that represent the recipient
amount | 4 bytes | the intended value as a single-precision float according to IEEE 754
currency | 3-4 bytes | the intended unit of account as an ascii string according to ISO 4217
timestamp | 4 bytes | the intended unix timestamp of the transaction as an unsigned big-endian integer

*Note 1: Transactions are signed by the senders private key to prevent impersonation*
*Note 2: Transactions are claims by the sender only, and should not be trusted blindly*

Wallets that display Transaction Intent data must:

- verify that the calculated exchange rate is within 1% of the wallets price data for the time of the transaction.
- verify that the timestamp provided is within 1 blocks time and not in the future.
- provide strong warning messages if expectations are not met.

Wallets may choose to apply more stringent checks.


### Private and Shared Intents

The message that is encrypted us the **Intent Type** and all **Intent Data**, structured as **Bitcoin Script** in the same order as they would've been inside the transaction output.

Field | Size | Description
--- | --- | ---
type | 1 bytes | the unencrypted intent type
data | 1~146 bytes | the data fields according to the type

